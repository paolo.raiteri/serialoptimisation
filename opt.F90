program test_opt
  implicit none
  integer :: ndim
  integer :: i, j, k, idx
  integer :: natoms
  real(8) :: box_size, volume, rho, norm
  real(8), dimension(3) :: dij
  real(8) :: distance, rcut, rcut2, dr
  real(8) :: t0, t1, t2, t3

  character(len=2), allocatable, dimension(:)   :: label
  real(8),          allocatable, dimension(:,:) :: position

  integer :: maxBins
  real(8), allocatable, dimension(:) :: distribution

  call cpu_time(t0)
  ! Number of dimensions
  ndim=3

  ! Read number of atoms
  read(*,*) natoms
  write(0,'("Number of atoms :",i0)')natoms
  allocate(label   (     natoms))
  allocate(position(ndim,natoms))

  ! Read box size and compute volume and number density
  read(*,*) box_size
  volume = box_size**3
  rho = natoms/volume

  !*** read positions & labels ***
  do i=1,natoms
    read(*,*) label(i), position(1:3,i)
  enddo

  ! Variables for the distribution
  maxBins=100
  allocate(distribution(maxBins))
  distance=0
  rcut=6.d0
  rcut2 = rcut**2
  dr=(rcut/maxBins)

  call cpu_time(t1)
  !*** pairwise distances ***
  do i=1,natoms
    do j=i+1,natoms
      dij(1:3) = position(1:3,j) - position(1:3,i)
      distance = sum(dij(1:3)*dij(1:3))
      ! Do something with the distances here
!      if (distance < rcut2) then
!      endif   
    enddo
  enddo
  call cpu_time(t2)
  
  ! Normalisation and output of the distribution
  call cpu_time(t3)

  write(0,'(a15,f10.3)')"total time  ",t3-t0
  write(0,'(a15,f10.3)')"reading time",t1-t0
  write(0,'(a15,f10.3)')"loop time   ",t2-t1
  write(0,'(a15,f10.3)')"writing time",t3-t2
end program test_opt
